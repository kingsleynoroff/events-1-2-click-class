# Basic click event 2

When the button on the page is clicked, *add* a CSS class to the button.  
The button already has a class on it, make sure that class isn't overriden.

The button has the class of *bigbutton* on it, when the button is clicked, add the class of *clickedbutton*.

*Note:* The styles are provided for you in master.css

This is the [link to example](https://events-1-2-click-class.now.sh)

## Task

Clone or download this repository onto your computer.  You will start out in the "master" branch which contains an empty project.

Try to recreate the website above.  Firstly, try to create it without any help.  If you are unsure of what to do, you can follow the steps below.  If the steps don't help, checkout out the "answer" branch from this repository.  The answer branch contains a working example.

## Steps

1. Add a button into the HTML, make sure to add the bigbutton class
2. Select the button in JavaScript
3. Add the event listener onto the button
4. Create a callback function that is passed into the event listener.  The function should add the clickedbutton class onto the button elements class list